Installation Instructions
=========================
The current state of th project does not yet warrant this.

Brainstorm
----------
That doesnt mean i cant think about it out loud.

Other than assembling the relevant sensors and sircuit boards, there are two
parts to the beholder build that needs to happen.

* collating all the relevant javascript modules
* building an arduino firmware that includes your sensors.

In addition to that you could build a fancy interface to the aggregate, but
that shouldnt be necessary for basic operation.


Dependencies
============

Aggregate
---------
* python >= 3.7
* python-requests
* uwsgi
* nginx
* systemd

Beholder
--------
* Arduino
* blah blah
